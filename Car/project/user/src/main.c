#include "zf_common_headfile.h"

int main (void)
{
    clock_init(SYSTEM_CLOCK_120M);
    debug_init();
   
    fivekey_init();
    tft180_init();
    mt9v03x_init();
    motor_init(17000);
    icm20602_init();
    gpio_init(H2, GPO, GPIO_HIGH, GPO_PUSH_PULL);//核心板上默认为不亮
    gpio_init(G10, GPO, GPIO_LOW, GPO_PUSH_PULL); //蜂鸣器
    uart_init(UART_5, 115200, UART5_TX_A4, UART5_RX_A5);
    fourpid_init();
    pit_ms_init(TIM2_PIT,10);
    interrupt_set_priority(TIM2_IRQn,2);
    while(1)
    {
        basic_state_machine();
        basic_state_machine_handler();
    }
}
//test_encoder();
//test_motor();