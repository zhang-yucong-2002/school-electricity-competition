#ifndef _menu_h_
#define _menu_h_
#include "zf_common_headfile.h"

void Pid2Str(void);
extern int selectItem_current,selectItem_hidden,selectItem;
extern const struct MenuItem MainMenu[];
extern const struct MenuItem* MenuPoint;
extern const struct MenuItem Setmenu0[];
extern const struct MenuItem Setmenu1[];
extern const struct MenuItem Setmenu2[];
extern const struct MenuItem Setmenu3[];
extern const struct MenuItem Setmenu4[];

int my_min(int a,int b);
void Menu_Init(void);
void Menu_Return(void);
void Display(const struct MenuItem * MenuPoint,int selectItem_current,int selectItem_hidden);
void Menu(void);
void test_A(void);


#endif

