#include "imu.h"
#include "zf_common_headfile.h"
#include "math.h"

extern float gyro_now;
extern int16 icm20602_gyro_z;//未转为标准单位

extern bool test_10ms_flag;

void imu_update(void)
{
    float x,y,z;
    icm20602_get_acc();
    icm20602_get_gyro();

    x=icm20602_gyro_transition(icm20602_gyro_x);
    y=icm20602_gyro_transition(icm20602_gyro_y);
    z=icm20602_gyro_transition(icm20602_gyro_z);

   // printf("%f,%f,%f\r\n",x,y,z);
}


void gyroz_update(void)
{
    icm20602_get_gyro();
    gyro_now=icm20602_gyro_transition(icm20602_gyro_z);
}
