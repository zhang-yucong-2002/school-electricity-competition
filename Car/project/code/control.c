#include "control.h"
#include "zf_common_headfile.h"
#include "pid.h"
pid_param_t leftmotor_pid,rightmotor_pid,gyro_pid,angle_pid;
//控制周期标志位
bool speed_flag=0,gyro_flag=0,angle_flag;
//速度环输入，编码器得到的真实转速cm/s
float speed_l_now=0,speed_r_now=0;
///pid输出/速度执行量
float speed_l=0,speed_r=0;
//速度环输入，期望速度
float speed_l_target=0,speed_r_target=0;
//基本前进速度
float basic_speed=200;
//差速
float speed_turn=0;

//角速度环输入，IMU得到的真实角速度
float gyro_now=0;
//角速度期望速度
float gyro_target=0;
//零漂
float gyro_offset=0;

extern float Beacon_X,Beacon_Y;
//显示的距离和时间
uint32 countsecond=0;
float distance=0;
bool update_time_flag;//更新时间标志位

//状态
Move_State_Enum move_state;
bool start_3s_flag=0; //开始计时标志位 1为开始计时，0为计时结束/不计时

Basic_State_Enum basic_state; //状态状态机

unsigned char beacon_count;//计数

void fourpid_init(void)
{
    pid_init(&leftmotor_pid);
    pid_init(&rightmotor_pid);
    pid_init(&gyro_pid);
    pid_init(&angle_pid);

    leftmotor_pid.kp=3.32;
    leftmotor_pid.ki=0.31;
    leftmotor_pid.out_limit=2500;

    rightmotor_pid.kp=3.35;
    rightmotor_pid.ki=0.18;
    rightmotor_pid.out_limit=2500;
    
    gyro_pid.kp = -7.15;
    gyro_pid.ki = -0.75;
    gyro_pid.out_limit=2500;

    angle_pid.kp = 2.5;
    angle_pid.out_limit=2000;
}

void control_system(void)
{
    if (speed_flag)
    {
        read_speed();
        distance+=(speed_l_now+speed_r_now)*0.0025;
        speed_l=pid_inc_ctrl(&leftmotor_pid,speed_l_target,speed_l_now);
        speed_r=pid_inc_ctrl(&rightmotor_pid,speed_r_target,speed_r_now);
        motor_speedset(speed_l,speed_r);
        speed_flag=0;
    }

    if(move_state!=Riding)
    {
        if(gyro_flag)
        {
            gyroz_update();
            speed_turn=pid_inc_ctrl(&gyro_pid,gyro_target,gyro_now);
            speed_l_target=basic_speed+speed_turn;
            speed_r_target=basic_speed-speed_turn;
            gyro_flag=0;
        }
        if(angle_flag)
        {
            gyro_target=pid_loc_ctrl(&angle_pid,65,Beacon_X);
            angle_flag=1;
        }
        SeekBeacon();
    }
}


void move_state_machine(void)
{
    if(Beacon_X!=0&&Beacon_Y!=0)
    {
     move_state=Searching;//寻灯状态
      if(Beacon_Y>70)
          basic_speed=90;
      else 
          basic_speed=325;
    }
    if(move_state==Searching&&Beacon_Y==0)
    {
         speed_l_target=0;
         speed_r_target=0;
         uart_write_byte(UART_5, 0xA7);
         beacon_count++;
         move_state=Riding;//骑灯开始
         pid_clean(&gyro_pid);
         pid_clean(&angle_pid);//清理积分
         start_3s_flag=1;//开始计时
        
    }
    if(move_state==Riding&&start_3s_flag==0)
    {
         move_state=Losing;//3骑灯结束状态，也是丢灯状态
         if(beacon_count==5)
        {
            beacon_count=0;
            exit_move_mode();
            basic_state=Init;
        }
    }

}




void basic_state_machine(void)
{
    
   switch (Key_num)
   {
       case Button1_SINGLE_CLICK:
       {
        Key_num=9;
        basic_state=Init;//初始
        tft180_set_dir(TFT180_PORTAIT);
        tft180_init();
        exit_move_mode();
        basic_page();//一开始显示这页，是因为key_num初始化为0
       }
       break;
       case Button5_SINGLE_CLICK:
       {
        Key_num=9;
        basic_state=Move;//运动
        tft180_set_dir(TFT180_PORTAIT);
        tft180_init();
        move_basic_page();
        pit_ms_init(TIM6_PIT,5);//控制中断
        interrupt_set_priority(TIM6_IRQn,1);
       }
       break;
       case Button3_SINGLE_CLICK:
       {
        Key_num=9;
        basic_state=Raw_Image;
        tft180_set_dir(TFT180_CROSSWISE_180);
        tft180_init();
        exit_move_mode();
       }
       break;
       case Button4_SINGLE_CLICK:
       {
        Key_num=9;
        basic_state=Processed_Image;
        tft180_set_dir(TFT180_CROSSWISE_180);
        tft180_init();
        exit_move_mode();
       }
   }
}

void basic_state_machine_handler(void)
{
    switch(basic_state)
    {
        case Init:
        {
            
        }
        break;
        case Move:
        {
             if(update_time_flag)
            {
                update_time_flag=0;
                move_update_page();
            }
                control_system();
                move_state_machine();
        }
        break;
        case Raw_Image:
        {
           show_raw_image();
        }
        break;
        case Processed_Image:
        {
            show_processed_image();
        }
        break;
    }
}


void basic_page(void)
{
    tft180_show_string(32,0,"2023.5.7");
    tft180_show_string(0,16,"ElectronicDesign");
    tft180_show_string(0,48,"Number:");
    tft180_show_string(20,64,"CS284912366");
    tft180_show_string(52,80,"C35");
    tft180_show_string(0,112,"Member:");
    tft180_show_string(20,128,"ZYC XCY ZCW");
}


//只刷新数字，不刷新字符
void move_basic_page(void)
{
    tft180_show_string(48,0,"Move");
    tft180_show_string(0,32,"Running time:");
    tft180_show_string(104,48,"sec");
    tft180_show_string(0,64,"Distance:");
    tft180_show_string(112,80,"cm");
}
void move_update_page(void)
{
    tft180_show_uint(70,48,countsecond,4);
    tft180_show_float(16,80,distance,7,1);
}

void exit_move_mode(void)
{
    pid_clean(&leftmotor_pid);
    pid_clean(&rightmotor_pid);
    pid_clean(&gyro_pid);
    pid_clean(&angle_pid);
    pit_disable(TIM6_PIT);//清理积分
    distance=0;
    motor_speedset(0,0);
}


