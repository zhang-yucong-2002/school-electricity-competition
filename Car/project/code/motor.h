#ifndef _motor_h_
#define _motor_h_

#include "zf_common_headfile.h"

void motor_init(uint16_t freq);
void motor_speedset(float speed_l_set,float speed_r_set);
void test_encoder(void);
void read_speed(void);
void test_motor(void);
void uart1_rx_interrupt_handler(void);
float Speed_Low_Filter(float new_Spe,float *speed_Record);
#endif