#include "zf_common_headfile.h"
#include "fivekeys.h"

#define key_up           B11
#define key_down         B10
#define key_left         E15
#define key_right        E14
#define key_middle       E13

struct Button button1,button2,button3,button4,button5;

enum Button_IDs {
    btn1_id,
    btn2_id,
    btn3_id,
    btn4_id,
    btn5_id,
};

char Key_num=0;

uint8_t read_button_GPIO(uint8_t button_id)
{
    switch(button_id)
        {
            case btn1_id:
                return gpio_get_level(key_up);
                break;
            case btn2_id:
                return gpio_get_level(key_down);
                break;
            case btn3_id:
                return gpio_get_level(key_left);
                break;
            case btn4_id:
                return gpio_get_level(key_right);
                break;
            case btn5_id:
                return gpio_get_level(key_middle);
                break;
            default:
                return 0;
        }
    return 0 ;
}

void fivekey_init(void)
{
    gpio_init(key_up,    GPI, 1, GPI_PULL_UP);
    gpio_init(key_down,  GPI, 1, GPI_PULL_UP);
    gpio_init(key_left,  GPI, 1, GPI_PULL_UP);
    gpio_init(key_right, GPI, 1, GPI_PULL_UP);
    gpio_init(key_middle,GPI, 1, GPI_PULL_UP);

    button_init(&button1,read_button_GPIO,0,btn1_id);
    button_init(&button2,read_button_GPIO,0,btn2_id);
    button_init(&button3,read_button_GPIO,0,btn3_id);
    button_init(&button4,read_button_GPIO,0,btn4_id);
    button_init(&button5,read_button_GPIO,0,btn5_id);


    button_attach(&button1, SINGLE_CLICK, button1_callback);
    button_attach(&button2, SINGLE_CLICK, button2_callback);
    button_attach(&button3, SINGLE_CLICK, button3_callback);
    button_attach(&button4, SINGLE_CLICK, button4_callback);
    button_attach(&button5, SINGLE_CLICK, button5_callback);
    
    button_start(&button1);
    button_start(&button2);
    button_start(&button3);
    button_start(&button4);
    button_start(&button5);
}

void button1_callback(void *button)
{
    uint32_t btn_event_val;

    btn_event_val = get_button_event((struct Button *)button);

    switch(btn_event_val)
    {
        case SINGLE_CLICK:
            Key_num=Button1_SINGLE_CLICK ;
           // printf("***> key1 single press hold! <***\r\n");
            break;
    }
}

void button2_callback(void *button)
{
    uint32_t btn_event_val;

    btn_event_val = get_button_event((struct Button *)button);

    switch(btn_event_val)
    {
        case SINGLE_CLICK:
            Key_num=Button2_SINGLE_CLICK ;
            //printf("***> key2 single press hold! <***\r\n");
            break;
    }
}

void button3_callback(void *button)
{
    uint32_t btn_event_val;

    btn_event_val = get_button_event((struct Button *)button);

    switch(btn_event_val)
    {
        case SINGLE_CLICK:
            Key_num=Button3_SINGLE_CLICK ;
            //printf("***> key3 single press hold! <***\r\n");
            break;
    }
}


void button4_callback(void *button)
{
    uint32_t btn_event_val;

    btn_event_val = get_button_event((struct Button *)button);

    switch(btn_event_val)
    {
        case SINGLE_CLICK:
            Key_num=Button4_SINGLE_CLICK ;
            //printf("***> key4 single press hold! <***\r\n");
            break;
    }
}

void button5_callback(void *button)
{
    uint32_t btn_event_val;

    btn_event_val = get_button_event((struct Button *)button);

    switch(btn_event_val)
    {
        case SINGLE_CLICK:
            Key_num=Button5_SINGLE_CLICK ;
            //printf("***> key5 single press hold! <***\r\n");
            break;
    }
}







