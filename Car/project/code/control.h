#ifndef _control_h_
#define _control_h_

void fourpid_init(void);
void move_state_machine(void);
void basic_state_machine(void);
void control_system(void);
void basic_page(void);
void move_basic_page(void);
void move_update_page(void);
void basic_state_machine_handler(void);
void exit_move_mode(void);
typedef enum {
    Init,//初始化 显示队伍信息的那个
    Move, //运动模式
    Raw_Image,//原始图模式
    Processed_Image,
}Basic_State_Enum;

typedef enum {
    Searching,//寻灯
    Riding, //骑灯
    Losing,//丢灯
}Move_State_Enum;



#endif

