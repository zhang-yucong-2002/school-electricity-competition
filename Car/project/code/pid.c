#include "zf_common_headfile.h"
#include "pid.h"

extern pid_param_t leftmotor_pid,rightmotor_pid,gyro_pid,angle_pid;

void pid_init(pid_param_t * pid)
{
  pid->kp        = 0;
  pid->ki        = 0;
  pid->kd        = 0;
  pid->imax      = 0;
  pid->out_p     = 0;
  pid->out_i     = 0;
  pid->out_d     = 0;
  pid->out       = 0;
  pid->integrator= 0;
  pid->last_error= 0;
  pid->pre_error = 0;
  pid->out_limit = 0;
}

void pid_clean(pid_param_t * pid)
{
  pid->out_p     = 0;
  pid->out_i     = 0;
  pid->out_d     = 0;
  pid->out       = 0;
  pid->integrator= 0;
  pid->last_error= 0;
  pid->pre_error = 0;
}

float pid_loc_ctrl(pid_param_t * pid, float Target,float Actual)
{
	float error;
	
  error=Target-Actual;
	
  pid->integrator +=pid->ki*error;
  
  if(pid->integrator > pid->imax)
     pid->out=pid->imax;
  else if(pid->out < - pid->imax)
          pid->out = - pid->imax;
  
  pid->out_p = pid->kp * error;
  pid->out_i = pid->integrator;
  pid->out_d = pid->kd * (error - pid->last_error);
  
  pid->last_error = error;
  
  pid->out = pid->out_p + pid->out_i + pid->out_d;
  
  if  (pid->out > pid->out_limit)
       pid->out=pid->out_limit;
  else if(pid->out < - pid->out_limit)
          pid->out = - pid->out_limit;


  return pid->out;
}


float pid_inc_ctrl(pid_param_t * pid, float Target,float Actual)
{
  
  float error;
	
 error=Target-Actual;
	
  pid->out_p = pid->kp * (error - pid->last_error);
  pid->out_i = pid->ki * error;
  pid->out_d = pid->kd * (error - 2*pid->last_error + pid->pre_error);
  
  pid->out += pid->out_p + pid->out_i + pid->out_d;//ע��˴��Ѿ�����ȥ��?

      if (pid->out > pid->out_limit)
          pid->out = pid->out_limit;
  else if(pid->out < - pid->out_limit)
          pid->out = - pid->out_limit;

  pid->pre_error = pid->last_error;
  pid->last_error = error;
  
  return pid->out;
}

