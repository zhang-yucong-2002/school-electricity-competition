#ifndef _pid_h_
#define _pid_h_

#include "zf_common_headfile.h"
#include <stdint.h>

typedef struct
{
  float                kp;         //P
  float                ki;         //I
  float                kd;         //D
  float                imax;       //�����޷�
  
  float                out_p;  //KP���
  float                out_i;  //KI���
  float                out_d;  //KD���
  float                out;    //pid���
  
  float                integrator; //< ����ֵ
  float                last_error; //< �ϴ����
  float                pre_error;  //< ���ϴ����
  float                out_limit;  //pid����޷�
}pid_param_t;


void pid_init(pid_param_t * pid);

void pid_clean(pid_param_t * pid);

float pid_loc_ctrl(pid_param_t * pid, float Target,float Actual);

float pid_inc_ctrl(pid_param_t * pid, float Target,float Actual);

#endif 
