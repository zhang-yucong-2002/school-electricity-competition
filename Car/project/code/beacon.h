#ifndef _beacon_h_
#define _beacon_h_

#include "zf_common_headfile.h"

#define led_rvs             gpio_toggle_level(H2)

void Two_Part_Binaryzation(unsigned char imageIn[MT9V03X_H][MT9V03X_W], unsigned char imageOut[MT9V03X_H][MT9V03X_W]);
void Two_Pass_V1(void);
void Scan_Window_Return_XY(void);
void Binaryzation(unsigned char imageIn[MT9V03X_H][MT9V03X_W], unsigned char imageOut[MT9V03X_H][MT9V03X_W]);//全局固定阈值
void show_processed_image (void);
void show_raw_image(void);
void SeekBeacon(void);//放在总函数里的
    
#endif