#include "zf_common_headfile.h"
#include "zf_device_tft180.h"
#include <stdio.h>
#include "menu.h"
#include "pid.h"
#include "menu.h"

extern pid_param_t leftmotor_pid,rightmotor_pid,gyro_pid,angle_pid;

/***默认使用8*16字体，如果不是该字体，需要自行探索一下下面的代码了，
需要用户移植时关心并替换，建议在menu最上方加入屏幕头文件，以防假警告，
封装过多的函数会降低效率，用define替换又会增加编译器负担，这没法兼顾捏***/
typedef enum
{
    motoL=2,
    motoR,
    gyro,
    angle,
}pid_location;

#define Menu_ClearAll()         (tft180_clear ())
#define Menu_ClearArea()        (tft180_clear_area (160,120))//比如有6行，使用8*16字体，那一共显示的内容只有240*96（不考虑横向）
#define Menu_ClearArrow()       (tft180_clear_area(16,160))//clear_area可以参考逐飞的clear函数进行编写
#define Menu_ShowStr(x,y,dat)   (tft180_show_string (x,y,dat))
#define linenumber              (6)    //屏幕希望用于展示的行数,溢出时，支持隐藏
#define Menu_ShowMark           ("->") //菜单用于提示的光标
#define Menu_ShowStartX         (18)   //菜单字符串起始位置，不是箭头，要考虑位置，上面的->是2*8=16，那起始必须大于16
//#define Pid_Show1               (LeftMotor_PID) //第一个需要展示的PID
//#define Pid_Show2               (RightMotor_PID)
//#define Pid_Show3               (Gyro_PID)
//#define Pid_Show4               (Angle_PID)
static char TempPIDstr[4][16];  //存放需要展示的PID参数，这里我展示p,i,d,max四项
//char Pid_MarkLocation=0;        //用来记忆进入了哪个PID的调整页面
//int Pid_MarkElement=0;         //用来记忆那个元素
//void Pid2Str(void)
//{
//    switch(selectItem_current)
//    {
//    case motoL :
//      Pid_MarkLocation=motoL;
//      sprintf((char*)TempPIDstr[0],"Kp = %4.2f",Pid_Show1.kp);
//      sprintf((char*)TempPIDstr[1],"Ki = %4.2f",Pid_Show1.ki);
//      sprintf((char*)TempPIDstr[2],"Kd = %4.2f",Pid_Show1.kd);
//      sprintf((char*)TempPIDstr[3],"Mx = %4.0f",Pid_Show1.out_limit);
//      break;
//    case motoR :
//        Pid_MarkLocation=motoR;
//      sprintf((char*)TempPIDstr[0],"Kp = %4.2f",Pid_Show2.kp);
//      sprintf((char*)TempPIDstr[1],"Ki = %4.2f",Pid_Show2.ki);
//      sprintf((char*)TempPIDstr[2],"Kd = %4.2f",Pid_Show2.kd);
//      sprintf((char*)TempPIDstr[3],"Mx = %4.0f",Pid_Show2.out_limit);
//      break;
//    case gyro :
//        Pid_MarkLocation= gyro;
//      sprintf((char*)TempPIDstr[0],"Kp = %4.2f",Pid_Show3.kp);
//      sprintf((char*)TempPIDstr[1],"Ki = %4.2f",Pid_Show3.ki);
//      sprintf((char*)TempPIDstr[2],"Kd = %4.2f",Pid_Show3.kd);
//      sprintf((char*)TempPIDstr[3],"Mx = %4.0f",Pid_Show3.out_limit);
//      break;
//    case angle :
//        Pid_MarkLocation= angle;
//      sprintf((char*)TempPIDstr[0],"Kp = %4.2f",Pid_Show4.kp);
//      sprintf((char*)TempPIDstr[1],"Ki = %4.2f",Pid_Show4.ki);
//      sprintf((char*)TempPIDstr[2],"Kd = %4.2f",Pid_Show4.kd);
//      sprintf((char*)TempPIDstr[3],"Mx = %4.0f",Pid_Show4.out_limit);
//      break;
//    default:
//       break;
//    }
//}

/****以下代码无需关心****/
struct MenuItem
{
    char *DisplayString;//当前项目要显示的字符
    void(*Subs)(void);//选择某一菜单后执行的功能函数
    const struct MenuItem *ChildrenMenus;//当前项目的子菜单
};
//菜单显示的行数
int selectItem_current,selectItem_hidden,selectItem;
const struct MenuItem *MenuPoint = MainMenu;//当前菜单
/****以上代码无需关心****/

/*****进入菜单需要展示的部分，根据需求进行更改*****/

//第一级
//第一项菜单 参数1为结构体数组的长度  参数2为NULL 参数3为父菜单
//剩下的菜单项 参数1为名字，参数2为子函数，参数3为子菜单
#define MainMenunum (intptr_t)7
const struct MenuItem MainMenu[MainMenunum] = {
{ (char*)MainMenunum,NULL ,NULL },
{ "Mode"  ,NULL ,Setmenu0},
{ "Lmotor_PID",NULL,Setmenu1},
{"Rmotor_PID",NULL,Setmenu2},
{"Gyro_PID",NULL,Setmenu3},
{"Angle_PID",NULL,Setmenu4},
{"To be added",NULL,NULL},
};

//第二级
//第一项菜单 参数1为结构体数组的长度  参数2为NULL 参数3为父菜单
//剩下的菜单项 参数1为名字，参数2为子函数，参数3为子菜单
#define Setmenu0num (intptr_t)5
const struct MenuItem Setmenu0[Setmenu0num]={
{ (char*)Setmenu0num,NULL ,MainMenu },
{"High Speed",test_A,NULL },
{"Middle Speed",NULL,NULL },
{"Low Speed",NULL,NULL },
{"Back"   ,Menu_Return,NULL },
};
//左电机
#define Setmenu1num (intptr_t)6
const struct MenuItem Setmenu1[Setmenu1num]={
{ (char*)Setmenu1num,NULL ,MainMenu },
{ TempPIDstr[0],NULL,Setmenu1},
{ TempPIDstr[1] ,NULL,NULL, },
{ TempPIDstr[2] ,NULL,NULL, },
{ TempPIDstr[3] ,NULL,NULL, },
{ "Back"   ,Menu_Return,NULL },
};
//右电机
#define Setmenu2num (intptr_t)6
const struct MenuItem Setmenu2[Setmenu2num]={
{ (char*)Setmenu2num,NULL ,MainMenu },
{ TempPIDstr[0] ,NULL,NULL, },
{ TempPIDstr[1] ,NULL,NULL, },
{ TempPIDstr[2] ,NULL,NULL, },
{ TempPIDstr[3] ,NULL,NULL, },
{ "Back"   ,Menu_Return,NULL },
};
//角速度环 增量式pi
#define Setmenu3num (intptr_t)6
const struct MenuItem Setmenu3[Setmenu3num]={
{ (char*)Setmenu3num,NULL ,MainMenu },
{ TempPIDstr[0], NULL,NULL, },
{ TempPIDstr[1] ,NULL,NULL, },
{ TempPIDstr[2] ,NULL,NULL, },
{ TempPIDstr[3] ,NULL,NULL, },
{ "Back"   ,Menu_Return,NULL },
};

#define Setmenu4num (intptr_t)6
const struct MenuItem Setmenu4[Setmenu4num]={
{ (char*)Setmenu4num,NULL ,MainMenu },
{ TempPIDstr[0], NULL,NULL, },
{ TempPIDstr[1] ,NULL,NULL, },
{ TempPIDstr[2] ,NULL,NULL, },
{ TempPIDstr[3] ,NULL,NULL, },
{ "Back"   ,Menu_Return,NULL },
};



void test_A(void)
{
    printf("High speed");
}



/*****以下为底层代码区，用户无需关心******/

//比较两个数，返回小的数
int my_min(int a,int b)
{
    if (a<b)
    {
    return a;
    }
    return b;
}

//菜单初始化并显示根目录内容
void Menu_Init(void)
{
    selectItem_current =1;
    selectItem_hidden  =0;
    selectItem=selectItem_current+selectItem_hidden;
    Menu_ClearAll();
    Display(MenuPoint,selectItem_current,selectItem_hidden);
}

//菜单显示函数
void Display(const struct MenuItem * MenuPoint,int selectItem_current,int selectItem_hidden)
{
    int j;//for循环用的
    uint16 line=0;//显示的起始行数
    Menu_ShowStr(0,16*(selectItem_current-1),Menu_ShowMark);//显示光标
    for ( j= selectItem_hidden+1; j < my_min((intptr_t)MenuPoint->DisplayString,linenumber+selectItem_hidden+1);j++)
    {
        Menu_ShowStr(Menu_ShowStartX,16*line,MenuPoint[j].DisplayString);
        line+=1;
    }
}

void Menu_Return(void)
{
    if(MenuPoint!=MainMenu&&MenuPoint[0].ChildrenMenus!=NULL)
    {
        Menu_ClearArea();
        MenuPoint = MenuPoint[0].ChildrenMenus;
        selectItem_current =1;
        selectItem_hidden  =0;
        selectItem=selectItem_current+selectItem_hidden;
        Display(MenuPoint,selectItem_current,selectItem_hidden);
    }
}

void Menu(void) //该函数请一直刷新
{
    switch(Key_num)
    {
        case Button1_SINGLE_CLICK:
        {//上
            Menu_ClearArrow();
            selectItem_current--;
            if (selectItem_current==0)
            {
                if(selectItem_hidden>0)
                    selectItem_hidden--;
                selectItem_current++;
            }
            selectItem=selectItem_current+selectItem_hidden;
            Display(MenuPoint,selectItem_current,selectItem_hidden);
        };break;
        case Button3_SINGLE_CLICK:
        {//下
            Menu_ClearArrow();
            selectItem_current++;
            if (selectItem_current+selectItem_hidden == (intptr_t)MenuPoint->DisplayString)//加上可实现往下移动跑到最上面一行
            {
                    selectItem_current =1;
                    selectItem_hidden  =0;
                    selectItem=selectItem_current+selectItem_hidden;
                    Display(MenuPoint,selectItem_current,selectItem_hidden);
            }
            if(selectItem_current>linenumber)//当前的行数超过总行数
            {
                if (selectItem_current+selectItem_hidden <= (intptr_t)MenuPoint->DisplayString)
                    selectItem_hidden++;
                selectItem_current--;
            }
            else if(selectItem_current>(intptr_t)MenuPoint->DisplayString)selectItem_current--;
            selectItem=selectItem_current+selectItem_hidden;
            Display(MenuPoint,selectItem_current,selectItem_hidden);
        };break;
        //右或中都可以作为确认
        case Button5_SINGLE_CLICK:
        case Button2_SINGLE_CLICK:
        {//中 确认进入此项目
            if((MenuPoint[selectItem].Subs != NULL)&&(MenuPoint[selectItem].ChildrenMenus != NULL))//当子函数与子菜单同时存在时，先运行子函数，后加载子页面
            {
                            MenuPoint[selectItem].Subs();
                            Menu_ClearArea();
                            MenuPoint = MenuPoint[selectItem].ChildrenMenus;
                            selectItem_current =1;
                            selectItem_hidden  =0;
                            selectItem=selectItem_current+selectItem_hidden;
                            Display(MenuPoint,selectItem_current,selectItem_hidden);

            }
            else if (MenuPoint[selectItem].ChildrenMenus != NULL)//判断是否有下一级
            {
                Menu_ClearArea();
                MenuPoint = MenuPoint[selectItem].ChildrenMenus;
                selectItem_current =1;
                selectItem_hidden  =0;
                selectItem=selectItem_current+selectItem_hidden;
                Display(MenuPoint,selectItem_current,selectItem_hidden);
            }
            else if(MenuPoint[selectItem].Subs != NULL)
            {
                MenuPoint[selectItem].Subs();
            }
        };break;
        case Button4_SINGLE_CLICK:
        {//左 返回上一级
            Menu_Return();
        }
    }
    Key_num = 9;

}


void init_systerm(void)
{
    tft180_init();
    mt9v03x_init();
    icm20602_init();
    led_init;
    buzzer_init;
    fivekey_init();
    motor_init(17000);
    encoder_quad_init(TIM3_ENCODER, TIM3_ENCODER_CH1_B4, TIM3_ENCODER_CH2_B5);
    encoder_quad_init(TIM4_ENCODER, TIM4_ENCODER_CH1_B6, TIM4_ENCODER_CH2_B7);
    pit_ms_init(TIM2_PIT,10);
    interrupt_set_priority(TIM2_IRQn,3);//中断使能已经在init中实现
    bee_on;
    system_delay_ms(300);
    bee_off;
}

