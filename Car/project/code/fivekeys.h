#ifndef _fivekeys_h_
#define _fivekeys_h_

#include "zf_common_headfile.h"

#define led_init     gpio_init(H2,GPO,GPIO_HIGH,GPO_PUSH_PULL)
#define buzzer_init  gpio_init(G10,GPO,GPIO_LOW,GPO_PUSH_PULL)
#define bee_on       gpio_set_level(G10,GPIO_HIGH)
#define bee_off      gpio_set_level(G10,GPIO_LOW)
#define led_on       gpio_set_level(H2,GPIO_HIGH)
#define led_off      gpio_set_level(H2,GPIO_LOW)
#define led_toggle   gpio_toggle_level(H2)

extern char Key_num;

typedef enum {
    Button1_SINGLE_CLICK,
    Button2_SINGLE_CLICK,
    Button3_SINGLE_CLICK,
    Button4_SINGLE_CLICK,
    Button5_SINGLE_CLICK,
}Buttons_Staus;


void fivekey_init(void);
uint8_t read_button_GPIO(uint8_t button_id);
void button1_callback(void *button);
void button2_callback(void *button);
void button3_callback(void *button);
void button4_callback(void *button);
void button5_callback(void *button);
void Basic_State_Machine(void);

#endif