/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define RU_Pin GPIO_PIN_1
#define RU_GPIO_Port GPIOA
#define Mid_Pin GPIO_PIN_3
#define Mid_GPIO_Port GPIOA
#define LU_Pin GPIO_PIN_4
#define LU_GPIO_Port GPIOA
#define LD_Pin GPIO_PIN_5
#define LD_GPIO_Port GPIOA
#define IR_RU_Pin GPIO_PIN_6
#define IR_RU_GPIO_Port GPIOA
#define IR_RD_Pin GPIO_PIN_7
#define IR_RD_GPIO_Port GPIOA
#define IR_Mid_Pin GPIO_PIN_0
#define IR_Mid_GPIO_Port GPIOB
#define IR_LU_Pin GPIO_PIN_1
#define IR_LU_GPIO_Port GPIOB
#define IR_LD_Pin GPIO_PIN_8
#define IR_LD_GPIO_Port GPIOA
#define RD_Pin GPIO_PIN_8
#define RD_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
