/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#define TIME 2400
typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned int uint32_t;

typedef uint32_t u32;
typedef uint16_t u16;  
typedef uint8_t u8;   
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
//初始化标志位 ，IR红外 ，LED信号灯
u8 LED_FLAG = 0;
u8 RX_FLAG=0;
unsigned char RX[1];
u8 cycle=0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void LED_IR_INIT();
void LED_Flash();
void IR_Receive();
void LED_IR_SET();


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
	LED_IR_INIT(); //初始化所有灯
	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_SET);
	HAL_UART_Receive_IT(&huart1,(unsigned char *)RX,1);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	//握手
//	while(1)
//	{
//		if(RX_FLAG==1)
//		{
//			
//			if(RX[0]==0XA5)//握手
//			{
//				HAL_UART_Transmit(&huart1,(uint8_t *)0XA6,1,HAL_MAX_DELAY);
//				HAL_UART_Receive_IT(&huart1,(unsigned char *)RX,1);
//				RX_FLAG=0;

//				break;
//			}
//			RX_FLAG=0;

//			HAL_UART_Receive_IT(&huart1,(unsigned char *)RX,1);		
//		}
//	}
	//
  while (1)
  {
		if(cycle==1&&LED_FLAG==1)
		{
			LED_FLAG++;
		}
		LED_Flash();
		if(RX_FLAG==1)	
		{
			if(RX[0]==0XA7)
			{
				IR_Receive();//切换
			}
			RX_FLAG=0;
			HAL_UART_Receive_IT(&huart1,(unsigned char *)RX,1);
		}
		
		if(LED_FLAG==5)
		{
			LED_FLAG=0;
			cycle++;
		}
			
//			LED_IR_INIT();//关灯结束		
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
//串口回调函数 
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance==USART1)//串口1
	{
		RX_FLAG=1;
	}
}


void LED_IR_INIT()
{
	//pull down all led
	HAL_GPIO_WritePin(RU_GPIO_Port,RU_Pin,GPIO_PIN_RESET);  
	HAL_GPIO_WritePin(RD_GPIO_Port ,RD_Pin,GPIO_PIN_RESET);  
	HAL_GPIO_WritePin(Mid_GPIO_Port,Mid_Pin,GPIO_PIN_RESET);  
	HAL_GPIO_WritePin(LU_GPIO_Port ,LU_Pin,GPIO_PIN_RESET);  
	HAL_GPIO_WritePin(LD_GPIO_Port ,LD_Pin,GPIO_PIN_RESET);  
}

void LED_IR_SET()
{
	//pull down all led
	HAL_GPIO_WritePin(RU_GPIO_Port,RU_Pin,GPIO_PIN_SET);  
	HAL_GPIO_WritePin(RD_GPIO_Port ,RD_Pin,GPIO_PIN_SET);  
	HAL_GPIO_WritePin(Mid_GPIO_Port,Mid_Pin,GPIO_PIN_SET);  
	HAL_GPIO_WritePin(LU_GPIO_Port ,LU_Pin,GPIO_PIN_SET);  
	HAL_GPIO_WritePin(LD_GPIO_Port ,LD_Pin,GPIO_PIN_SET);  
}




void LED_Flash()
{
	switch(LED_FLAG)
	{
		case 0:{HAL_GPIO_WritePin(LD_GPIO_Port,LD_Pin,GPIO_PIN_SET);break;}
		case 1:{HAL_GPIO_WritePin(Mid_GPIO_Port,Mid_Pin,GPIO_PIN_SET);break;}
		case 2:{HAL_GPIO_WritePin(LU_GPIO_Port,LU_Pin,GPIO_PIN_SET);break;}
		case 3:{HAL_GPIO_WritePin(RU_GPIO_Port,RU_Pin,GPIO_PIN_SET);break;}
		case 4:{HAL_GPIO_WritePin(RD_GPIO_Port,RD_Pin,GPIO_PIN_SET);break;}
		default:{break;}
	}
}

void IR_Receive()
{
	switch(LED_FLAG)
	{
		case 0:
		{
			LED_FLAG++;//下一个灯亮
			for(u8 i=0;i<6;i++)
			{
				HAL_GPIO_TogglePin(LD_GPIO_Port,LD_Pin);
				HAL_Delay(50);
			}
			HAL_Delay(TIME);//延时3s

			HAL_GPIO_WritePin(LD_GPIO_Port,LD_Pin,GPIO_PIN_RESET);  //关灯
			break;
		}
		case 1:
		{

			LED_FLAG++;//下一个灯亮
			for(u8 i=0;i<6;i++)
			{
				HAL_GPIO_TogglePin(Mid_GPIO_Port,Mid_Pin);
				HAL_Delay(50);
			}
			HAL_Delay(TIME);//延时3s
			HAL_GPIO_WritePin(Mid_GPIO_Port,Mid_Pin,GPIO_PIN_RESET);  //关灯
			break;
		}
		case 2:
		{

			LED_FLAG++;//下一个灯亮
			for(u8 i=0;i<6;i++)
			{
				HAL_GPIO_TogglePin(LU_GPIO_Port,LU_Pin);
				HAL_Delay(50);
			}
			HAL_Delay(TIME);//延时3s
			HAL_GPIO_WritePin(LU_GPIO_Port,LU_Pin,GPIO_PIN_RESET);  //关灯

			break;
		}	
		case 3:
		{

			LED_FLAG++;//下一个灯亮
			for(u8 i=0;i<6;i++)
			{
				HAL_GPIO_TogglePin(RU_GPIO_Port,RU_Pin);
				HAL_Delay(50);
			}
			HAL_Delay(TIME);//延时3s
			HAL_GPIO_WritePin(RU_GPIO_Port,RU_Pin,GPIO_PIN_RESET);  //关灯

			break;
		}
		case 4:
		{

			LED_FLAG++;//下一个灯亮
			for(u8 i=0;i<6;i++)
			{
				HAL_GPIO_TogglePin(RD_GPIO_Port,RD_Pin);
				HAL_Delay(50);
			}
			HAL_Delay(TIME);//延时3s
			HAL_GPIO_WritePin(RD_GPIO_Port,RD_Pin,GPIO_PIN_RESET);  //关灯

			break;
		}
		default:{break;}
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

